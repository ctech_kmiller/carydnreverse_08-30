﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.BoolValueEventArgs
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System;
using System.Runtime.InteropServices;

namespace CaryDN
{
    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.None)]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330023")]
    [ProgId("CaryDN.BoolValueEventArgs")]
    public class BoolValueEventArgs : EventArgs, IBoolValueEventArgs
    {
        #region Constructors

        public BoolValueEventArgs(bool value)
        {
            Value = value;
        }

        #endregion

        #region Properties and Indexers

        public bool Value { get; set; }

        #endregion
    }
}