﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.Helpers.ResourceHelper
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

namespace CaryDN.Helpers
{
  [ComVisible(false)]
  public class ResourceHelper
  {
    public static string GetDllName()
    {
      return ResourceHelper.GetDllName(Assembly.GetExecutingAssembly());
    }

    public static string GetDllName(Type type)
    {
      return ResourceHelper.GetDllName(Assembly.GetAssembly(type));
    }

    public static string GetDllName(Assembly assembly)
    {
      return assembly.FullName.Split(',')[0];
    }

    public static bool ResourceExists(string resourceName)
    {
      return Assembly.GetExecutingAssembly().GetManifestResourceInfo(resourceName) != null;
    }

    public static bool ResourceExists(Type type, string resourceName)
    {
      return ResourceHelper.ResourceExists(Assembly.GetAssembly(type), resourceName);
    }

    public static bool ResourceExists(Assembly assembly, string resourceName)
    {
      return assembly.GetManifestResourceInfo(resourceName) != null;
    }

    public static string GetString(string resourceID)
    {
      return ResourceHelper.GetString(resourceID, "ResourcesEN-AU");
    }

    public static string GetString(string resourceID, string resourceFileName)
    {
      Assembly executingAssembly = Assembly.GetExecutingAssembly();
      return ResourceHelper.GetString(string.Format("{0}.Resources.{1}", (object) ResourceHelper.GetDllName(executingAssembly), (object) resourceFileName), executingAssembly, resourceID);
    }

    public static string GetString(string baseName, Assembly assembly, string resourceID)
    {
      string str = new ResourceManager(baseName, assembly)
      {
        IgnoreCase = true
      }.GetString(resourceID);
      if (string.IsNullOrEmpty(str))
        return resourceID;
      return str;
    }
  }
}
