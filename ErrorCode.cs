﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.ErrorCode
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System.Runtime.InteropServices;
using CaryDN.Helpers;

// ReSharper disable UnusedMember.Global
// ReSharper disable InconsistentNaming

namespace CaryDN
{
    [ComVisible(false)]
    public class ErrorCode
    {
        #region Static Fields and constants

        public const string K_RESOURCE_ID_PREFIX = "E";

        #endregion

        #region Other Members

        public static string GetPrefix(int errorCode)
        {
            if (smethod_0(9000, 9099, errorCode))
                return "REX";
            if (smethod_0(9201, 9241, errorCode))
                return "MSG";
            if (smethod_0(9300, 9399, errorCode))
                return "SEQ";
            if (smethod_0(9400, 9499, errorCode))
                return "CHG";
            if (smethod_0(9500, 9599, errorCode))
                return "SIG";
            if (smethod_0(9600, 9699, errorCode))
                return "NMP";
            if (smethod_0(9700, 9799, errorCode))
                return "MTH";
            if (smethod_0(9800, 9899, errorCode))
                return "CCT";
            return string.Empty;
        }

        private static bool smethod_0(int int0, int int1, int int2)
        {
            return int2 >= int0 && int2 <= int1;
        }

        public static string ToResourceID(int errorCode)
        {
            return $"{(object) "E"}{(object) errorCode:0###}";
        }

        public static string ToString(int errorCode)
        {
            var prefix = GetPrefix(errorCode);
            var str = ResourceHelper.GetString(ToResourceID(errorCode));
            if (string.IsNullOrEmpty(str))
                str = $"Cary error '{(object) errorCode}'";
            return $"{(object) prefix}:{(object) str}";
        }

        #endregion
    }
}