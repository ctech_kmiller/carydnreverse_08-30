﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.ScanMode
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

// ReSharper disable InconsistentNaming

namespace CaryDN
{
    public enum ScanMode
    {
        Wavelength,
        Time,
        Peaking,
        WavelengthSNR,
        TimeSNR,
        Idle,
        PMVPeak,
        TCalibration,
        WaveNumber,
        WaveNumberSNR
    }
}