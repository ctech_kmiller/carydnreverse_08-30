﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.AccyStatus
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

namespace CaryDN
{
    public enum AccyStatus
    {
        SampleTransport = 1,
        Motor1 = 2,
        Motor2 = 4,
        Ramping1 = 8,
        Ramping2 = 16,                   // 0x00000010
        WaitingForStartTemperature = 32, // 0x00000020
        TemperatureUnreachable = 64,     // 0x00000040
        TemperatureReached = 128,        // 0x00000080
        WaitingForHoldTime = 256,        // 0x00000100
        A10 = 512,                       // 0x00000200
        A11 = 1024,                      // 0x00000400
        MonitorReceived = 2048,          // 0x00000800
        MeasureReceived = 4096,          // 0x00001000
        AccessoriesOffline = 8192,       // 0x00002000
        CommandBusy = 16384,             // 0x00004000
        CommandReceived = 32768          // 0x00008000
    }
}