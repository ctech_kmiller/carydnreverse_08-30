﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.InstrumentEx
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System;
using System.ComponentModel;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using System.Windows.Forms;

namespace CaryDN
{
    [ComDefaultInterface(typeof(IInstrumentEx))]
    [ProgId("CaryDN.InstrumentEx")]
    [ComVisible(true)]
    [ComSourceInterfaces(typeof(IInstrumentEvents))]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330000")]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    public class InstrumentEx : InstrumentBase, IInstrumentEx
    {
        #region Fields

        private Form0 _hiddenForm;
        private bool _initialized;
        private Thread _instrumentExThread;
        private ManualResetEvent _manualResetEvent0;

        #endregion

        #region Constructors

        public InstrumentEx()
        {
            Accessory = new Accessory();
            //Registration = new Registration();
        }

        #endregion

        #region Properties and Indexers

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category("Accessory")]
        public Accessory Accessory { get; }

        #endregion

        #region Interface Implementations

        public override void Dispose()
        {
            base.Dispose();
            Disposing();
        }

        public override bool Connect()
        {
            //CheckRegistrationKey(Registration.Key);
            if (_instrumentExThread == null)
                StartThread();
            return base.Connect();
        }

        #endregion

        #region Other Members

        ///// <summary>
        /////     Check registration key to see if it is valid, and display a <see cref="MessageBox" /> if not.
        ///// </summary>
        //// ReSharper disable once UnusedMember.Local
        //private void CheckRegistrationKey(string regKey)
        //{
        //    if (string.IsNullOrEmpty(regKey))
        //    {
        //        if (Registration.TrialDaysLeft != 0)
        //            return;
        //        MessageBox.Show("CaryDN dll trial period has expired", "...", MessageBoxButtons.OK,
        //            MessageBoxIcon.Exclamation);
        //    }
        //    else
        //    {
        //        // ReSharper disable once CollectionNeverUpdated.Local
        //        var registrationKeys = new RegistrationKeys();
        //        try
        //        {
        //            if (!registrationKeys.Contains(regKey))
        //                throw new Exception("Invalid key.");
        //        }
        //        finally
        //        {
        //            registrationKeys.Clear();
        //        }
        //    }
        //}
        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        private void Disposing()
        {
            _initialized = false;
            if (_instrumentExThread == null)
                return;
            Thread.Sleep(250);
            try
            {
                _instrumentExThread.Join(1000);
                if (_instrumentExThread.IsAlive)
                    _instrumentExThread.Abort();
            }
            catch (Exception ex)
            {
                //added KSM 2018-08-31
                Console.WriteLine(ex);
                throw;
                //end of additions}
            }

            _instrumentExThread = null;
        }

        private void form0_0_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        private void Initialize()
        {
            try
            {
                _initialized = true;
                _hiddenForm = new Form0(this);
                _hiddenForm.FormClosed += form0_0_FormClosed;
                ThreadStart?.Invoke(null, null);
                _manualResetEvent0.Set();
                while (_initialized)
                {
                    Application.DoEvents();
                    Thread.Sleep(250);
                }

                if (_hiddenForm == null)
                    return;
                _hiddenForm.Close();
                _hiddenForm.Dispose();
                _hiddenForm = null;
            }
            catch (Exception ex)
            {
                //added KSM 2018-08-31
                Console.WriteLine(ex);
                throw;
                //end of additions 
            }
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        private void StartThread()
        {
            using (_manualResetEvent0 = new ManualResetEvent(false))
            {
                _instrumentExThread = new Thread(Initialize)
                {
                    Name = nameof(InstrumentEx),
                    IsBackground = true
                };
                _instrumentExThread.SetApartmentState(ApartmentState.STA);
                _instrumentExThread.Start();
                _manualResetEvent0.WaitOne();
            }
        }

        public event EventHandler ThreadStart;

        #endregion

        #region

        [ComVisible(false)]
        private sealed class Form0 : Form
        {
            #region Fields

            private readonly InstrumentEx _instrumentEx;

            #endregion

            #region Constructors

            public Form0(InstrumentEx instrumentEx1)
            {
                _instrumentEx = instrumentEx1;
                _instrumentEx.WindowHandle = (int) Handle;
                _instrumentEx.Accessory.SetHandle((int) Handle);
            }

            #endregion

            #region Other Members

            protected override void WndProc(ref Message msg)
            {
                if (_instrumentEx != null)
                {
                    _instrumentEx.Processmessage(msg);
                    _instrumentEx.Accessory?.ProcessMessage(msg);
                }

                base.WndProc(ref msg);
            }

            #endregion
        }

        #endregion

        //[TypeConverter(typeof(ExpandableObjectConverter))]
        //[Category("Registration")]
        //public Registration Registration { get; }

        // ReSharper disable FieldCanBeMadeReadOnly.Local
        // ReSharper restore FieldCanBeMadeReadOnly.Local
    }
}