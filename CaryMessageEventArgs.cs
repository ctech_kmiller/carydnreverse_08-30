﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.CaryMessageEventArgs
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CaryDN
{
    [ProgId("CaryDN.CaryMessageEventArgs")]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [ComVisible(true)]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330015")]
    public class CaryMessageEventArgs : EventArgs, ICaryMessageEventArgs
    {
        #region Fields

        // naming copied from Startek source code
        // ReSharper disable once InconsistentNaming
        private readonly Message m_message;

        #endregion

        #region Constructors

        public CaryMessageEventArgs(Message mMessage)
        {
            m_message = mMessage;
        }

        #endregion

        #region Properties and Indexers

        public float FParam
        {
            get
            {
                Struct0 struct0;
                struct0.float_0 = 0.0f;
                struct0.int_0 = LParam;
                return struct0.float_0;
            }
        }

        public int LParam => m_message.LParam.ToInt32();

        public int LParamHi => (LParam >> 16) & ushort.MaxValue;

        public int LParamLo => LParam & ushort.MaxValue;

        public UserMessage MessageID => (UserMessage) m_message.Msg;

        public int WParam => m_message.WParam.ToInt32();

        #endregion

        #region Interface Implementations

        public override string ToString()
        {
            return
                $"Msg {m_message.Msg as object} ({m_message.Msg - 1024 as object}) WParam {m_message.WParam as object}, LParam {m_message.LParam as object}";
        }

        #endregion

        #region Other Members

        internal Message method_0()
        {
            return m_message;
        }

        #endregion

        #region

        [StructLayout(LayoutKind.Explicit)]
        private struct Struct0
        {
            [FieldOffset(0)] public float float_0;
            [FieldOffset(0)] public int int_0;
        }

        #endregion
    }
}