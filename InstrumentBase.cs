﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.InstrumentBase
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.ExceptionServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using System.Windows.Forms;
using CaryDN.SCary32;

// ReSharper disable ArrangeAccessorOwnerBody

namespace CaryDN
{
    [ComVisible(false)]
    public class InstrumentBase : IDisposable, INotifyPropertyChanged
    {
        #region Public Delegates

        public delegate void Cary32MessageHandler();

        #endregion

        #region Fields

        private readonly Cary32MessageHandler _cary32MessageHandler;

        #endregion

        #region Constructors

        internal InstrumentBase()
        {
            _cary32MessageHandler = DoApplicationEvents;
        }

        #endregion

        #region Properties and Indexers

        [Category("Misc")]
        [Description("")]
        [DisplayName("Abscissa Interval")]
        public float AbscissaInterval
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_ABSCISSA_INTERVAL); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_ABSCISSA_INTERVAL, value); }
        }

        [Description("")]
        [Category("Misc")]
        [DisplayName("Abscissa Start")]
        public float AbscissaStart
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_ABSCISSA_START); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_ABSCISSA_START, value); }
        }

        [Category("UV Visible")]
        [DisplayName("Adjusted Averaging")]
        [Description("The average read time in milli seconds.")]
        public int Averaging
        {
            get
            {
                return (int) (float) Math.Round(GetCaryParameterValue(InstrumentParameterID.RESP_UVVIS_AVERAGING) /
                                                (double) ChopperCycles * 1000.0);
            }
            set
            {
                // ReSharper disable once PossibleLossOfFraction
                float avg = value * ChopperCycles / 1000;
                if (avg < 1.0)
                    avg = 1f;
                TryCarySetParameterValue(InstrumentParameterID.RESP_UVVIS_AVERAGING, avg);
            }
        }

        [Description("BeamMode controls the instrument’s beam arrangement.")]
        [Category("Lamps")]
        [DisplayName("Beam Mode")]
        [DefaultValue(4)]
        public BeamMode BeamMode
        {
            get { return (BeamMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_BEAM_MODE); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_BEAM_MODE, (float) value); }
        }

        [Description("")]
        [Category("Misc")]
        [DisplayName("Callibration Mode")]
        public CalibrationMode CallibrationMode
        {
            get { return (CalibrationMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_CAL_MODE); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_CAL_MODE, (float) value); }
        }

        [Category("Misc")]
        [Description("")]
        [DisplayName("Chopper Cycles")]
        public int ChopperCycles
        {
            get { return Model == CaryModel.Cary50 || Model == CaryModel.Cary60 ? 80 : 30; }
        }

        [DisplayName("Collect Pending")]
        [Description("")]
        [Category("Misc")]
        public float CollectPending
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_COLLECT_PENDING); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_COLLECT_PENDING, value); }
        }

        [Description("The timeout (milli sec) used when connecting to the Cary.")]
        public int ConnectionTimeout { get; set; } = 500;

        [Description("")]
        [DisplayName("Current Detector")]
        [Category("Detector")]
        public DetectorMode CurrentDetector
        {
            get { return (DetectorMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_CURRENT_DETECTOR); }
        }

        [Category("Filter")]
        [Description("")]
        [DisplayName("Current Filter")]
        public int CurrentFilter
        {
            get { return GetCaryParameterValueAsInt(InstrumentParameterID.RESP_CURRENT_FILTER); }
        }

        [Description("")]
        [DisplayName("Current Grating")]
        [Category("Grating")]
        public GratingMode CurrentGrating
        {
            get { return (GratingMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_CURRENT_GRATING); }
        }

        [Description("The current light source.")]
        [DisplayName("Current Source")]
        [Category("Misc")]
        public int CurrentSource
        {
            get { return GetCaryParameterValueAsInt(InstrumentParameterID.RESP_CURRENT_SOURCE); }
        }

        [DisplayName("Detector")]
        [Description("")]
        [Category("Detector")]
        public DetectorMode Detector
        {
            get { return (DetectorMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_DETECTOR); }
        }

        [DefaultValue(900)]
        [Category("Detector")]
        [Description("This is the detector changeover wavelength. Default value is 900 nm")]
        [DisplayName("Detector Changeover")]
        public float DetectorChangeoverWavelength
        {
            get { return GetCaryParameterValueAsInt(InstrumentParameterID.RESP_DETECTOR_CHANGEOVER_WAVELENGTH); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_DETECTOR_CHANGEOVER_WAVELENGTH, value); }
        }

        [DisplayName("Filter")]
        [Category("Filter")]
        [Description("")]
        public int Filter
        {
            get { return GetCaryParameterValueAsInt(InstrumentParameterID.RESP_FILTER); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_FILTER, value); }
        }

        [Category("Detector")]
        [Description("")]
        [DisplayName("Gain")]
        public int Gain
        {
            get { return GetCaryParameterValueAsInt(InstrumentParameterID.RESP_GAIN); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_GAIN, value); }
        }

        [DisplayName("Grating Changeover")]
        [Description("This is the grating changeover wavelength.")]
        [Category("Grating")]
        public float GratingChangeoverWavelength
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_GRATING_CHANGEOVER_WAVELENGTH); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_GRATING_CHANGEOVER_WAVELENGTH, value); }
        }

        [Description("This is the instrument parameter RESP_GRATING")]
        [DisplayName("Grating Mode")]
        [Category("Grating")]
        public GratingMode GratingMode
        {
            get { return (GratingMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_GRATING); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_GRATING, (float) value); }
        }

        [DisplayName("Instrument Time")]
        [Category("Misc")]
        [Description("This is the instrument parameter RESP_INSTRUMENT_TIME")]
        public float InstrumentTime
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_INSTRUMENT_TIME); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_INSTRUMENT_TIME, value); }
        }

        [Category("Status")]
        [Description("Indicates if the instrument is busy setting up or collecting data.")]
        public bool IsBusy
        {
            get
            {
                if (!IsConnected)
                    return false;
                return Scary.CaryBusy();
            }
        }

        [Description("This is the instrument parameter RESP_COLLECTING")]
        [DisplayName("IsCollecting")]
        [Category("Status")]
        public bool IsCollecting
        {
            get { return IsParameterValueZero(InstrumentParameterID.RESP_COLLECTING); }
        }

        [Description("")] [Category("Status")] public bool IsConnected { get; private set; }

        [DisplayName("IsOffLine")]
        [Description("This is the instrument parameter RESP_OFFLINE")]
        [Category("Status")]
        public bool IsOffLine
        {
            get { return IsParameterValueZero(InstrumentParameterID.RESP_OFFLINE); }
        }

        [Description("This property tracks the UM_CARY_OFFLINE, UM_CARY_ONLINE messages.")]
        [Category("Status")]
        public bool IsOnline { get; private set; }

        [DisplayName("IsResetting")]
        [Description(
            "This is the instrument parameter RESP_RESETTING. This is true after power on whilst the Cary is resetting.")]
        [Category("Status")]
        public bool IsResetting
        {
            get { return IsParameterValueZero(InstrumentParameterID.RESP_RESETTING); }
        }

        [Description("Returns the instrument scanning status.")]
        [Category("Status")]
        public bool IsScanning
        {
            get
            {
                if (!IsConnected)
                    return false;
                return Scary.CaryScanning();
            }
        }

        [DisplayName("Third Lamp On")]
        [Category("Lamps")]
        [Description("This is the status of the third lamp.")]
        public bool IsThirdLampOn
        {
            get { return CaryTest(InstrumentParameterID.RESP_SOURCE_ON_OFF_THIRD, 1); }
            set
            {
                if (value)
                    TryCarySetParameterValue(InstrumentParameterID.RESP_SOURCE_ON_OFF_THIRD, 1f);
                else
                    TryCarySetParameterValue(InstrumentParameterID.RESP_SOURCE_ON_OFF_THIRD, 0.0f);
            }
        }

        [Description("This is the status of the Ultra Violet lamp.")]
        [DisplayName("UV Lamp On")]
        [Category("Lamps")]
        public bool IsUvLampOn
        {
            get { return CaryTest(InstrumentParameterID.RESP_SOURCE_ON_OFF_UV, 1); }
            set
            {
                if (value)
                    TryCarySetParameterValue(InstrumentParameterID.RESP_SOURCE_ON_OFF_UV, 1f);
                else
                    TryCarySetParameterValue(InstrumentParameterID.RESP_SOURCE_ON_OFF_UV, 0.0f);
            }
        }

        [DisplayName("VIS Lamp On")]
        [Description("This is the status of the visible light spectrum lamp.")]
        [Category("Lamps")]
        public bool IsVisibleLampOn
        {
            get { return CaryTest(InstrumentParameterID.RESP_SOURCE_ON_OFF_VIS, 1); }
            set
            {
                if (value)
                    TryCarySetParameterValue(InstrumentParameterID.RESP_SOURCE_ON_OFF_VIS, 1f);
                else
                    TryCarySetParameterValue(InstrumentParameterID.RESP_SOURCE_ON_OFF_VIS, 0.0f);
            }
        }

        [Category("Lamps")]
        [Description("This is also named Lamp1Time.")]
        [DisplayName("UV Lamp Time")]
        public float Lamp1Time
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAMP1_TIME); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_LAMP1_TIME, value); }
        }

        [Description("The Visible lamp time. This is also named Lamp2Time.")]
        [DisplayName("VIS Lamp Time")]
        [Category("Lamps")]
        public float Lamp2Time
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAMP2_TIME); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_LAMP2_TIME, value); }
        }

        [Category("Lamps")]
        [DisplayName("Third Lamp Time")]
        [Description("This is also named Lamp3Time.")]
        public float Lamp3Time
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAMP3_TIME); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_LAMP3_TIME, value); }
        }

        [DisplayName("Last D2 Peak")]
        [Category("Lamps")]
        [Description("")]
        public float LastD2PeakWavelength
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAST_D2PEAK_WAVELENGTH); }
        }

        [Category("Detector")]
        [Description("DAC output controlling the EHT voltage on the PMT, 0..255")]
        [DisplayName("Last Pmv Gain Index")]
        public float LastPmvGainIndex
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAST_PMV_GAIN_INDEX); }
        }

        [DisplayName("Last read value")]
        [Category("Misc")]
        [Description("")]
        public float LastReadValue
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAST_RD_VALUE); }
        }

        [Description("Last Rear Raw T value")]
        [DisplayName("LastRearTValue")]
        [Category("Misc")]
        public float LastRearTValue
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAST_REART_VALUE); }
        }

        [DisplayName("Last slit width")]
        [Description("")]
        [Category("Slit")]
        public float LastSlitWidth
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAST_SLIT_WIDTH); }
        }

        [Description("")]
        [Category("Misc")]
        [DisplayName("Last Time")]
        public float LastTime
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAST_TIME); }
        }

        [Category("Misc")]
        [DisplayName("Last Transmission Value")]
        [Description("")]
        public float LastTransmissionValue
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_LAST_TRANSMISSION_VALUE); }
        }

        [Category("Misc")]
        [Description("The instrument model number.")]
        [DisplayName("Model")]
        public CaryModel Model
        {
            get
            {
                var num = GetCaryParameterValueAsInt(InstrumentParameterID.RESP_MODEL);
                if (num < 0 || num > 10)
                    return CaryModel.Unknown;
                return (CaryModel) num;
            }
        }

        [Description(
            "NIRAveraging is the number of chopper cycles (1/30 s) over which the measurement should be averaged.")]
        [Category("Near Infra Red")]
        [DisplayName("NIR Averaging")]
        public int NIRAveraging
        {
            get
            {
                return (int) (float) Math.Round(
                    (float) (GetCaryParameterValue(InstrumentParameterID.RESP_NIR_AVERAGING) / (double) ChopperCycles *
                             1000.0));
            }
            set
            {
                // ReSharper disable once PossibleLossOfFraction
                float avg = value * ChopperCycles / 1000;
                if (avg < 1.0)
                    avg = 1f;
                TryCarySetParameterValue(InstrumentParameterID.RESP_NIR_AVERAGING, avg);
            }
        }

        [DisplayName("NIR Detector Calibration")]
        [Description("")]
        [Category("Near Infra Red")]
        public float NIRDetectorCalibration
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_NIR_DETECTOR_CALIBRATION); }
        }

        [DefaultValue(0)]
        [Description(
            "NIRInterval is the wavelength interval (nm) between consecutive points in an NIR wavelength scan.")]
        [Category("Near Infra Red")]
        [DisplayName("NIR Interval")]
        public float NIRInterval
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_NIR_INTERVAL); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_NIR_INTERVAL, value); }
        }

        [Category("Near Infra Red")]
        [Description("")]
        [DisplayName("NIR Peak Wavelengths")]
        public float NIRPeakWavelengths
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_NIR_PEAK_WAVELENGTHS); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_NIR_PEAK_WAVELENGTHS, value); }
        }

        [DisplayName("NIR Ref Level")]
        [Description("")]
        [Category("Near Infra Red")]
        public float NIRRefLevel
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_NIR_REF_LEVEL); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_NIR_REF_LEVEL, value); }
        }

        [Description("This is the slit width (nm) for an NIR scan. The default value is 4.")]
        [DisplayName("NIR Slit Width")]
        [Category("Near Infra Red")]
        [DefaultValue(4)]
        public float NIRSlitWidth
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_NIR_SLIT_WIDTH); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_NIR_SLIT_WIDTH, value); }
        }

        [Description("")]
        [DisplayName("NIR Wavelength Calib A")]
        [Category("Near Infra Red")]
        public float NIRWavelengthCalibA
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_NIR_WAVELENGTH_CALIB_A); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_NIR_WAVELENGTH_CALIB_A, value); }
        }

        [Description("")]
        [Category("Near Infra Red")]
        [DisplayName("NIR Wavelength Calib B")]
        public float NIRWavelengthCalibB
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_NIR_WAVELENGTH_CALIB_B); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_NIR_WAVELENGTH_CALIB_B, value); }
        }

        [Description("")]
        [Category("Near Infra Red")]
        [DisplayName("NIR Wavelength Calib C")]
        public float NIRWavelengthCalibC
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_NIR_WAVELENGTH_CALIB_C); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_NIR_WAVELENGTH_CALIB_C, value); }
        }

        [DisplayName("PGA Gain Indices Dark")]
        [Category("PGA")]
        [Description("")]
        public float PgaGainIndicesDark
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_PGA_GAIN_INDICES_DARK); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_PGA_GAIN_INDICES_DARK, value); }
        }

        [DisplayName("PGA Gain Indices Front")]
        [Category("PGA")]
        [Description("")]
        public float PgaGainIndicesFront
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_PGA_GAIN_INDICES_FRONT); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_PGA_GAIN_INDICES_FRONT, value); }
        }

        [Category("PGA")]
        [Description("")]
        [DisplayName("PGA Gain Indices Rear")]
        public float PgaGainIndicesRear
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_PGA_GAIN_INDICES_REAR); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_PGA_GAIN_INDICES_REAR, value); }
        }

        [Description("Programable amplifier gain mode.")]
        [Category("PGA")]
        [DisplayName("PGA Mode  ")]
        public PGAMode PgaMode
        {
            get { return (PGAMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_PGA_MODE); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_PGA_MODE, (float) value); }
        }

        [Category("Scan")]
        [Description("This reflects the instrument ScanMode only after the scan starts.")]
        [ReadOnly(true)]
        [DisplayName("Scan mode")]
        public ScanMode ScanMode
        {
            get { return (ScanMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_SCAN_MODE); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_SCAN_MODE, (float) value); }
        }

        [Category("Scan")]
        [Description("")]
        [DisplayName("Scan points")]
        public float ScanPoints
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SCAN_POINTS); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_SCAN_POINTS, value); }
        }

        [Description("")]
        [DisplayName("Scan Start")]
        [DefaultValue(500)]
        [Category("Scan")]
        public float ScanStart
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SCAN_START); }
            set { SetCaryParameter(InstrumentParameterID.RESP_SCAN_START, value); }
        }

        [Description("")]
        [Category("Scan")]
        [DisplayName("Scan Stop")]
        public float ScanStop
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SCAN_STOP); }
            set { SetCaryParameter(InstrumentParameterID.RESP_SCAN_STOP, value); }
        }

        [Description("")]
        [DisplayName("Shutter Front")]
        [Category("Shutter")]
        public float ShutterFront
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SHUTTER_FRONT); }
        }

        [DisplayName("Shutter Rear")]
        [Description("")]
        [Category("Shutter")]
        public float ShutterRear
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SHUTTER_REAR); }
        }

        [DisplayName("Shutters Open Closed Front")]
        [Category("Shutter")]
        [Description("Shutters Open Closed Front")]
        public float ShuttersOpenClosedFront
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SHUTTERS_OPEN_CLOSED_FRONT); }
        }

        [Category("Shutter")]
        [DisplayName("Shutters Open Closed Rear")]
        [Description("Shutters Open Closed Rear")]
        public float ShuttersOpenClosedRear
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SHUTTERS_OPEN_CLOSED_REAR); }
        }

        [Description("")]
        [Category("Misc")]
        [DisplayName("Sig Procc Calib Gain")]
        public float SigProccCalibGain
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SIG_PROCC_CALIB_GAIN); }
            set { SetCaryParameter(InstrumentParameterID.RESP_SIG_PROCC_CALIB_GAIN, value); }
        }

        [Category("Misc")]
        [DisplayName("Sig Procc Calib Offset")]
        [Description("")]
        public float SigProccCalibOffset
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SIG_PROCC_CALIB_OFFSET); }
            set { SetCaryParameter(InstrumentParameterID.RESP_SIG_PROCC_CALIB_OFFSET, value); }
        }

        [Category("Simulator")]
        [DisplayName("Simulator Mode")]
        public bool SimulatorMode { get; set; }

        [Description("")]
        [Category("Misc")]
        [DisplayName("Slit Height")]
        public SlitHeight SlitHeight
        {
            get { return (SlitHeight) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_SLIT_HEIGHT); }
            // ReSharper disable once ValueParameterNotUsed
            set { SetCaryParameter(InstrumentParameterID.RESP_SLIT_HEIGHT, (float) SlitHeight); }
        }

        [Description("This is the lamp source changeover wavelength.")]
        [DisplayName("Source Changeover")]
        [Category("Lamps")]
        public float SourceChangeoverWavelength
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_SOURCE_CHANGEOVER_WAVELENGTH); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_SOURCE_CHANGEOVER_WAVELENGTH, value); }
        }

        [Category("Lamps")]
        [DisplayName("Source Mode")]
        [Description("")]
        public SourceMode SourceMode
        {
            get { return (SourceMode) GetCaryParameterValueAsInt(InstrumentParameterID.RESP_SOURCE); }
            set { SetCaryParameter(InstrumentParameterID.RESP_SOURCE, (float) value); }
        }

        [Description("")]
        [DisplayName("Status")]
        [Category("Misc")]
        public float Status
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_STATUS); }
        }

        [Description("")]
        [DisplayName("Target Signal Noise")]
        [Category("Misc")]
        public float TargetSignalNoise
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_TARGET_SIGNAL_NOISE); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_TARGET_SIGNAL_NOISE, value); }
        }

        [Category("Misc")]
        [Description("")]
        [DisplayName("Timer")]
        public float Timer
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_TIMER); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_TIMER, value); }
        }

        [Description("This is the unadjusted averaging read time in chopper cycles.")]
        [Category("UV Visible")]
        [DisplayName("Averaging")]
        public int UvVisAveraging
        {
            get { return GetCaryParameterValueAsInt(InstrumentParameterID.RESP_UVVIS_AVERAGING); }
            set { SetCaryParameter(InstrumentParameterID.RESP_UVVIS_AVERAGING, value); }
        }

        [Description("UV Visible interval")]
        [Category("UV Visible")]
        [DisplayName("Interval")]
        public float UvVisInterval
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_UVVIS_INTERVAL); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_UVVIS_INTERVAL, value); }
        }

        [Category("UV Visible")]
        [Description("This is the slit width (nm) for a UV/Vis scan. The default value is 1.")]
        [DisplayName("Slit Width")]
        public float UvVisSlitWidth
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_UVVIS_SLIT_WIDTH); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_UVVIS_SLIT_WIDTH, value); }
        }

        [Category("UV Visible")]
        [DisplayName("Wave Number Interval")]
        [Description("")]
        public float UvVisWaveNumberInterval
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_UVVIS_WAVENUMBER_INTERVAL); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_UVVIS_WAVENUMBER_INTERVAL, value); }
        }

        [Description("This is the hardware version for Cary 50/60.")]
        [Browsable(false)]
        [Category("Misc")]
        public float VersionMain
        {
            get { return GetCaryParameterValue(InstrumentParameterID.RESP_VERSION_MAIN); }
        }

        [Category("Scan")]
        [Description(
            "The wavelength (nm)  that the instrument will drive to when the Setup() method is invoked. The default value is 500")]
        [DisplayName("Wavelength")]
        public float Wavelength
        {
            get { return GetCaryParameterValueAsInt(InstrumentParameterID.RESP_GOTO_WAVELENGTH); }
            set { TryCarySetParameterValue(InstrumentParameterID.RESP_GOTO_WAVELENGTH, value); }
        }

        [ReadOnly(true)] public int WindowHandle { get; set; } = -1;

        #endregion

        #region Interface Implementations

        public virtual void Dispose()
        {
            if (IsConnected)
                Disconnect();
            Scary.CommsSetAbortFlag(WindowHandle, true);
            Scary.CommsSetTerminated(WindowHandle);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Other Members

        public event CaryInfoEventHandler CaryInfoReceived;

        public float CaryReading(float wavelength)
        {
            return Scary.CaryReading(WindowHandle, wavelength);
        }

        public bool CaryTest(InstrumentParameterID id, int value)
        {
            return GetCaryParameterValueAsInt(id) == value;
        }

        public virtual bool Connect()
        {
            Connect(ConnectionTimeout, SimulatorMode);
            return IsConnected;
        }

        public virtual void Connect(int timeOut, bool simulatorMode)
        {
            Connecting?.Invoke(this, new EventArgs());
            Scary.SetCommsDebug(false);
            IsOnline = false;
            IsConnected = Scary.AttachComms(WindowHandle, timeOut, simulatorMode);
            if (!IsConnected)
                throw new Exception("Connect() failed.");
            Cary32.SetProcessMessagesProc(_cary32MessageHandler);
            ScaryAddResponseDevice(DeviceAddress.Instrument, 8192U, true, UserMessage.UM_CARY_OFFLINE);
            ScaryAddResponseDevice(DeviceAddress.Instrument, 8194U, false, UserMessage.UM_CARY_ONLINE);
            ScaryLog(DeviceAddress.Instrument, UserMessage.UM_CARY_DATA_IN, UserMessage.UM_CARY_ERROR_IN,
                UserMessage.UM_CARY_STATUS_IN, UserMessage.UM_CARY_VALUE_IN);
            ScaryLog(DeviceAddress.Accessory, UserMessage.UM_ACCY_DATA_IN, UserMessage.UM_ACCY_ERROR_IN,
                UserMessage.UM_ACCY_STATUS_IN, 0);
            ScaryLog(DeviceAddress.SamplePreparation, UserMessage.UM_SPS_DATA_IN, UserMessage.UM_SPS_ERROR_IN,
                UserMessage.UM_SPS_STATUS_IN, 0);
            GetModel();
            Connected?.Invoke(this, new EventArgs());
        }

        public event EventHandler Connected;

        public event EventHandler Connecting;

        public event DataEventHandler DataReceived;

        public virtual void Disconnect()
        {
            Disconnecting?.Invoke(this, new EventArgs());
            Cary32.SetProcessMessagesProc(null);
            Scary.CommsSetAbortFlag(WindowHandle, false);
            Scary.CaryStop(WindowHandle);
            RemoveDeviceResponse(DeviceAddress.Instrument, 8192U);
            RemoveDeviceResponse(DeviceAddress.Instrument, 8194U);
            ScaryUnlog(DeviceAddress.Instrument);
            ScaryUnlog(DeviceAddress.Accessory);
            ScaryUnlog(DeviceAddress.SamplePreparation);
            Scary.DetachComms(WindowHandle);
            IsOnline = false;
            IsConnected = false;
            Disconnected?.Invoke(this, new EventArgs());
        }

        public event EventHandler Disconnected;

        public event EventHandler Disconnecting;

        private void DoApplicationEvents()
        {
            Application.DoEvents();
        }

        public event ErrorEventHandler Error;

        // ReSharper disable once UnusedMember.Local
        private Enum GetCaryParameterAsEnum(InstrumentParameterID instrumentParameterID, Type type)
        {
            var num = GetCaryParameterValueAsInt(instrumentParameterID);
            return (Enum) Enum.Parse(type, num.ToString());
        }

        internal float GetCaryParameterValue(InstrumentParameterID instrumentParameterID)
        {
            return Scary.CaryGet((int) instrumentParameterID);
        }

        internal int GetCaryParameterValueAsInt(InstrumentParameterID instrumentParameterID)
        {
            return (int) GetCaryParameterValue(instrumentParameterID);
        }

        public void GetDefaultSetup()
        {
            Scary.CaryGetDefaultSetup();
        }

        public string GetErrorMessage(int errorCode)
        {
            return ErrorCode.ToString(errorCode);
        }

        public void GetModel()
        {
            SubSetup(SubSetupCode.SUB_PROM_MODEL);
            Thread.Sleep(100);
        }

        public void GetStatus()
        {
            SubSetup(SubSetupCode.SUB_STATUS);
            Thread.Sleep(100);
        }

        public void GotoWavelength(float wavelength)
        {
            method_11();
            Stop();
            TryCarySetParameterValue(InstrumentParameterID.RESP_GOTO_WAVELENGTH, wavelength);
            Setup();
        }

        internal bool IsParameterValueZero(InstrumentParameterID instrumentParameterID)
        {
            return GetCaryParameterValueAsInt(instrumentParameterID) != 0;
        }

        private void method_11()
        {
            if (!IsConnected)
                throw new Exception("Not connected to instrument");
        }

        // ReSharper disable once UnusedMember.Local
        private void method_12(CaryMessageEventArgs args)
        {
            // ReSharper disable once UnusedVariable
            var lparam1 = args.LParam;
            // ReSharper disable once UnusedVariable
            var lparam2 = args.LParam;
        }

        internal bool method_14(Message msg)
        {
            var flag = false;
            switch ((UserMessage) msg.Msg)
            {
                case UserMessage.WM_FCARY_BASE:
                case UserMessage.UM_FCARY_STATUS:
                case UserMessage.UM_FCARY_INFO:
                case UserMessage.UM_FCARY_PARAM_WL:
                case UserMessage.UM_FCARY_TEST:
                case UserMessage.UM_FCARY_TEST_STATE:
                    flag = true;
                    break;
            }

            return flag;
        }

        // ReSharper disable once UnusedMember.Local
        // ReSharper disable once UnusedParameter.Local
        private float method_9(InstrumentParameterID instrumentParameterID, float value)
        {
            return GetCaryParameterValue(instrumentParameterID);
        }

        public event BoolValueEventHandler OnlineChanged;

        public event CaryMessageEventHandler ParamWL;

        private void PassMessageToFCary(Message msg)
        {
            switch ((UserMessage) msg.Msg)
            {
                case UserMessage.WM_FCARY_BASE:
                case UserMessage.UM_FCARY_STATUS:
                case UserMessage.UM_FCARY_INFO:
                case UserMessage.UM_FCARY_PARAM_WL:
                case UserMessage.UM_FCARY_TEST:
                case UserMessage.UM_FCARY_TEST_STATE:
                    Scary.FCaryMessageSink(msg.Msg, (int) msg.LParam);
                    break;
            }
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        internal virtual void Processmessage(Message msg)
        {
            PassMessageToFCary(msg);
            switch ((UserMessage) msg.Msg)
            {
                case UserMessage.UM_FCARY_INFO:
                    CaryInfoReceived?.Invoke(this, new CaryInfoEventArgs(msg));
                    break;
                case UserMessage.UM_FCARY_PARAM_WL:
                    ParamWL?.Invoke(this, new CaryMessageEventArgs(msg));
                    break;
                case UserMessage.UM_CARY_DATA_IN:
                    if (!Scary.CaryRead(out var wavelength, out var frontBeam, out var rearBeam) ||
                        DataReceived == null) break;
                    DataReceived(this, new DataEventArgs(wavelength, frontBeam, rearBeam));
                    break;
                case UserMessage.UM_CARY_STATUS_IN:
                    StatusChanged?.Invoke(this, new StatusEventArgs(msg));
                    break;
                case UserMessage.UM_CARY_ERROR_IN:
                    Error?.Invoke(this, new ErrorEventArgs(msg));
                    break;
                case UserMessage.UM_CARY_OFFLINE:
                    IsOnline = false;
                    OnlineChanged?.Invoke(this, new BoolValueEventArgs(IsOnline));
                    break;
                case UserMessage.UM_CARY_RESET:
                    Resetting?.Invoke(this, new EventArgs());
                    break;
                case UserMessage.UM_CARY_VALUE_IN:
                    ValueInReceived?.Invoke(this, new ValueInEventArgs(msg));
                    break;
                case UserMessage.UM_CARY_ONLINE:
                    IsOnline = true;
                    OnlineChanged?.Invoke(this, new BoolValueEventArgs(IsOnline));
                    break;
            }
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        protected virtual void RaisePropertyChanged(PropertyChangedEventArgs args)
        {
            var changedEventHandler0 = PropertyChanged;
            changedEventHandler0?.Invoke(this, args);
        }

        protected void RaisePropertyChanged(string propertyName)
        {
            RaisePropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        public void Read(out float wavelength, out float frontBeam, out float rearBeam)
        {
            method_11();
            if (!Scary.CaryRead(out wavelength, out frontBeam, out rearBeam))
                throw new Exception("CaryRead()");
        }

        public float Read(float wavelength)
        {
            method_11();
            return Scary.CaryReading(WindowHandle, wavelength);
        }

        public void RemoveDeviceResponse(DeviceAddress device, uint status)
        {
            Scary.RemoveResponseDevice(WindowHandle, (int) device, status);
        }

        public event EventHandler Resetting;

        internal void ScaryAddResponseDevice(DeviceAddress device, uint status, bool transition, UserMessage messageID)
        {
            if (!Scary.AddResponseDevice(WindowHandle, (int) device, status, transition, (int) messageID))
                throw new Exception($"AddDeviceResponse() error for device = {device}");
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        internal void ScaryLog(DeviceAddress deviceAddress, UserMessage userMessage0, UserMessage userMessage1,
            UserMessage userMessage2, UserMessage userMessage3)
        {
            try
            {
                Scary.Log(WindowHandle, (byte) deviceAddress, (int) userMessage0, (int) userMessage1,
                    (int) userMessage2, (int) userMessage3);
            }
            catch (Exception ex)
            {
                //added KSM 2018-08-31
                Console.WriteLine(ex);
                throw;
            }
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        internal void ScaryUnlog(DeviceAddress deviceAddress)
        {
            try
            {
                Scary.UnLog(WindowHandle, (byte) deviceAddress);
            }
            catch (Exception ex)
            {
                //added KSM 2018-08-31
                Console.WriteLine(ex);
                throw;
            }
        }

        internal bool SetCaryParameter(InstrumentParameterID instrumentParameterID, float @float)
        {
            method_11();
            return Scary.CarySet(WindowHandle, (int) instrumentParameterID, @float);
        }

        public void SetCommsDebug(bool onOff)
        {
            Scary.SetCommsDebug(onOff);
        }

        public void SetSimFile(string fileName)
        {
            if (!new FileInfo(fileName).Exists)
                throw new FileNotFoundException(string.Format("The file '{0}' could not be found.", fileName),
                    fileName);
            Scary.CarySetSimFile(fileName);
        }

        public void Setup()
        {
            method_11();
            if (!Scary.CarySetup(WindowHandle))
                throw new Exception("CarySetup()");
        }

        public void Start()
        {
            if (!Scary.CaryStart(WindowHandle))
                throw new Exception("CaryStart()");
        }

        public event StatusEventHandler StatusChanged;

        public void Stop()
        {
            if (!Scary.CaryStop(WindowHandle))
                throw new Exception("CaryStop()");
        }

        public void SubSetup(SubSetupCode code)
        {
            if (!Scary.CarySubSetup(WindowHandle, (int) code))
                throw new Exception(string.Format("SubSetup Exception, code = {0}", code.ToString()));
        }

        // ReSharper disable once UnusedMethodReturnValue.Local
        private bool TryCarySetParameterValue(InstrumentParameterID instrumentParameterID, float value)
        {
            var flag = true;
            var num = GetCaryParameterValue(instrumentParameterID);
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (value != (double) num && (flag = Scary.CarySet(WindowHandle, (int) instrumentParameterID, value)))
                RaisePropertyChanged(instrumentParameterID.ToString());
            return flag;
        }

        public event ValueInEventHandler ValueInReceived;

        public void WaitNotBusy()
        {
            Scary.CaryWaitNotBusy();
        }

        public void WaitNotScanning()
        {
            Scary.CaryWaitNotScanning();
        }

        #endregion

        // ReSharper disable InconsistentNaming
        public const int DB_OFF = 0;

        public const int DB_ON = 1;
        // ReSharper restore InconsistentNaming
    }
}