﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.IAccessoryEvents
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System.Runtime.InteropServices;

namespace CaryDN
{
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    [ComVisible(true)]
    [Guid("31F0B69F-E4BC-4328-9B0C-2DFDE3340002")]
    public interface IAccessoryEvents
    {
        [DispId(1)]
        void DataReceived(object sender, AccessoryDataEventArgs e);

        [DispId(2)]
        void Error(object sender, ErrorEventArgs e);

        [DispId(3)]
        void StatusChanged(object sender, StatusEventArgs e);
    }
}