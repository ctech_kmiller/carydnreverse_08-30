﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.DataEventArgs
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System;
using System.Runtime.InteropServices;

namespace CaryDN
{
    [ProgId("CaryDN.DataEventArgs")]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [ComVisible(true)]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330019")]
    public class DataEventArgs : EventArgs
    {
        #region Constructors

        public DataEventArgs(float wavelength, float frontBeam, float rearBeam)
        {
            Wavelength = wavelength;
            FrontBeam = frontBeam;
            RearBeam = rearBeam;
        }

        #endregion

        #region Properties and Indexers

        public virtual float Abs
        {
            get
            {
                var num = 9.999;
                if (FrontBeam > 0.0)
                    num = -Math.Log10(FrontBeam);
                return (float) num;
            }
        }

        public float FrontBeam { get; set; }

        public float RearBeam { get; set; }

        public float Wavelength { get; set; }

        #endregion

        #region Other Members

        public override string ToString()
        {
            return
                $"Abs={(object) Abs:0.####}, Wavelength={(object) Wavelength}, FrontBeam={(object) FrontBeam:0.####}, RearBeam={(object) RearBeam:0.####}";
        }

        #endregion
    }
}