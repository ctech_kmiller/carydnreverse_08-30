﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.ErrorEventArgs
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CaryDN
{
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330014")]
    [ProgId("CaryDN.ErrorEventArgs")]
    [ClassInterface(ClassInterfaceType.None)]
    [ComVisible(true)]
    public class ErrorEventArgs : CaryMessageEventArgs, IErrorEventArgs
    {
        #region Constructors

        public ErrorEventArgs(Message mMessage)
            : base(mMessage)
        {
        }

        #endregion

        #region Properties and Indexers

        public int ErrorCode => LParam;

        #endregion

        #region Interface Implementations

        public string GetErrorMessage()
        {
            return CaryDN.ErrorCode.ToString(ErrorCode);
        }

        #endregion
    }
}