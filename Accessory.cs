﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.Accessory
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using CaryDN.SCary32;

namespace CaryDN
{
    [ComDefaultInterface(typeof(IAccessory))]
    [ComVisible(true)]
    [ComSourceInterfaces(typeof(IAccessoryEvents))]
    [ProgId("CaryDN.Accessory")]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330002")]
    public class Accessory : IDisposable, IAccessory
    {
        #region Fields

        private int _handle = -1;

        #endregion

        #region Interface Implementations

        public bool Drive(int device, int condition, int device2, int value, int state)
        {
            return Scary.DriveAcc(_handle, device, condition, device2, value, state);
        }

        public bool EnableDrum(bool value)
        {
            return Scary.FCaryEnableDrumAcc(value);
        }

        public bool EnableXSlide(bool value)
        {
            return Scary.FCaryEnableXSlideAcc(value);
        }

        public bool Condition(int device, int value, int state)
        {
            return Scary.ConditionAcc(_handle, device, value, state);
        }

        public float GetParameter(int item)
        {
            return Scary.AccyGet(item);
        }

        public bool GotoCell(int cellNumber)
        {
            return Scary.GotoCellAcc(_handle, cellNumber);
        }

        public bool GoBusy(int device, int value, int state)
        {
            return Scary.GoBusyAcc(_handle, device, value, state);
        }

        public bool Limits(int device, int lowerLimit, int upperLimit)
        {
            return Scary.LimitAcc(_handle, device, lowerLimit, upperLimit);
        }

        public int Measure(int device)
        {
            return Scary.MeasureAcc(_handle, device);
        }

        public bool Monitor(int device1, int device2, int device3, int device4, int timeVal)
        {
            return Scary.MonitorAcc(_handle, device1, device2, device3, device4, timeVal);
        }

        public bool Ramp(int device, int rate)
        {
            return Scary.RampAcc(_handle, device, rate);
        }

        public bool RampA(int device, int rate)
        {
            return Scary.RampAAcc(_handle, device, rate);
        }

        public bool Reset(int item)
        {
            return Scary.AccyReSet(_handle, item);
        }

        public bool SetParameter(int item, float value)
        {
            return Scary.AccySet(_handle, item, value);
        }

        public bool Stop()
        {
            return Scary.StopAcc(_handle);
        }

        public void WaitNotBusy()
        {
            Scary.AccyWaitNotBusy();
        }

        public void Dispose()
        {
        }

        #endregion

        #region Other Members

        public event AccessoryDataEventHandler DataReceived;

        [Description(" Occurs on Accessory error.")]
        public event ErrorEventHandler Error;

        internal int method_0()
        {
            return _handle;
        }

        internal virtual void ProcessMessage(Message msg)
        {
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (msg.Msg)
            {
                case 1727: // UM_ACCY_DATA_IN
                    DataReceived?.Invoke(this, new AccessoryDataEventArgs(msg));
                    break;
                case 1728: // UM_ACCY_STATUS_IN
                    StatusChanged?.Invoke(this, new StatusEventArgs(msg));
                    break;
                case 1729: // UM_ACCY_ERROR_IN
                    Error?.Invoke(this, new ErrorEventArgs(msg));
                    break;
            }
        }

        internal void SetHandle(int handle)
        {
            _handle = handle;
        }

        public event StatusEventHandler StatusChanged;

        #endregion
    }
}