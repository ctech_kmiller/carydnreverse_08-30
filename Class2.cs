﻿// Decompiled with JetBrains decompiler
// Type: Class2
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;

internal static class Class2
{
  private static Class2.Class6 class6_0 = new Class2.Class6();

  internal static long smethod_0()
  {
    if ((object) Assembly.GetCallingAssembly() != (object) typeof (Class2).Assembly || !Class2.smethod_1())
      return 0;
    lock (Class2.class6_0)
    {
      long long_0 = Class2.class6_0.method_0();
      if (long_0 == 0L)
      {
        Assembly executingAssembly = Assembly.GetExecutingAssembly();
        List<byte> byteList = new List<byte>();
        AssemblyName assemblyName;
        try
        {
          assemblyName = executingAssembly.GetName();
        }
        catch
        {
          assemblyName = new AssemblyName(executingAssembly.FullName);
        }
        byte[] numArray = assemblyName.GetPublicKeyToken();
        if (numArray != null && numArray.Length == 0)
          numArray = (byte[]) null;
        if (numArray != null)
          byteList.AddRange((IEnumerable<byte>) numArray);
        byteList.AddRange((IEnumerable<byte>) Encoding.Unicode.GetBytes(assemblyName.Name));
        int num1 = Class2.smethod_3(typeof (Class2));
        int num2 = Class2.Class5.smethod_0();
        byteList.Add((byte) (num1 >> 24));
        byteList.Add((byte) (num2 >> 16));
        byteList.Add((byte) (num1 >> 8));
        byteList.Add((byte) num2);
        byteList.Add((byte) (num1 >> 16));
        byteList.Add((byte) (num2 >> 8));
        byteList.Add((byte) num1);
        byteList.Add((byte) (num2 >> 24));
        int count = byteList.Count;
        ulong num3 = 0;
        for (int index = 0; index != count; ++index)
        {
          ulong num4 = num3 + (ulong) byteList[index];
          ulong num5 = num4 + (num4 << 20);
          num3 = num5 ^ num5 >> 12;
          byteList[index] = (byte) 0;
        }
        ulong num6 = num3 + (num3 << 6);
        ulong num7 = num6 ^ num6 >> 22;
        long_0 = (long) (num7 + (num7 << 30)) ^ -3087388525442042740L;
        Class2.class6_0.method_1(long_0);
      }
      return long_0;
    }
  }

  private static bool smethod_1()
  {
    return Class2.smethod_2();
  }

  private static bool smethod_2()
  {
    StackFrame frame = new StackTrace().GetFrame(3);
    MethodBase methodBase = frame == null ? (MethodBase) null : frame.GetMethod();
    Type type = (object) methodBase == null ? (Type) null : methodBase.DeclaringType;
    return (object) type != (object) typeof (RuntimeMethodHandle) && (object) type != null && (object) type.Assembly == (object) typeof (Class2).Assembly;
  }

  private static int smethod_3(Type type_0)
  {
    return type_0.MetadataToken;
  }

  private sealed class Class3
  {
    internal static int smethod_0()
    {
      return Class2.Class9.smethod_0(Class2.smethod_3(typeof (Class2.Class3)), Class2.Class9.smethod_2(Class2.Class9.smethod_1(Class2.smethod_3(typeof (Class2.Class10)), Class2.smethod_3(typeof (Class2.Class5))), Class2.Class9.smethod_2(Class2.smethod_3(typeof (Class2.Class4)) ^ -1631687906, Class2.Class10.smethod_0())));
    }
  }

  private sealed class Class4
  {
    internal static int smethod_0()
    {
      return Class2.Class9.smethod_2(Class2.Class9.smethod_0(Class2.Class7.smethod_0() ^ 527758446, Class2.smethod_3(typeof (Class2.Class8))), Class2.Class9.smethod_1(Class2.smethod_3(typeof (Class2.Class5)) ^ Class2.smethod_3(typeof (Class2.Class3)), 1237543489));
    }
  }

  private sealed class Class5
  {
    internal static int smethod_0()
    {
      return Class2.Class9.smethod_2(Class2.Class9.smethod_1(Class2.smethod_3(typeof (Class2.Class7)), Class2.Class9.smethod_2(Class2.smethod_3(typeof (Class2.Class5)), Class2.smethod_3(typeof (Class2.Class10)))), Class2.Class3.smethod_0());
    }
  }

  private sealed class Class6
  {
    private int int_0;
    private int int_1;

    internal Class6()
    {
      this.method_1(0L);
    }

    internal long method_0()
    {
      if ((object) Assembly.GetCallingAssembly() != (object) typeof (Class2.Class6).Assembly || !Class2.smethod_1())
        return 2918384;
      int[] numArray = new int[4]{ 0, 0, 0, -363138076 };
      numArray[1] = 2085721899;
      numArray[2] = -995399669;
      numArray[0] = 49594187;
      int int0 = this.int_0;
      int int1 = this.int_1;
      int num1 = -1640531527;
      int num2 = -957401312;
      for (int index = 0; index != 32; ++index)
      {
        int1 -= (int0 << 4 ^ int0 >> 5) + int0 ^ num2 + numArray[num2 >> 11 & 3];
        num2 -= num1;
        int0 -= (int1 << 4 ^ int1 >> 5) + int1 ^ num2 + numArray[num2 & 3];
      }
      for (int index = 0; index != 4; ++index)
        numArray[index] = 0;
      return (long) ((ulong) int1 << 32 | (ulong) (uint) int0);
    }

    internal void method_1(long long_0)
    {
      if ((object) Assembly.GetCallingAssembly() != (object) typeof (Class2.Class6).Assembly || !Class2.smethod_1())
        return;
      int[] numArray = new int[4]{ 0, 2085721899, 0, 0 };
      numArray[0] = 49594187;
      numArray[2] = -995399669;
      numArray[3] = -363138076;
      int num1 = -1640531527;
      int num2 = (int) long_0;
      int num3 = (int) (long_0 >> 32);
      int num4 = 0;
      for (int index = 0; index != 32; ++index)
      {
        num2 += (num3 << 4 ^ num3 >> 5) + num3 ^ num4 + numArray[num4 & 3];
        num4 += num1;
        num3 += (num2 << 4 ^ num2 >> 5) + num2 ^ num4 + numArray[num4 >> 11 & 3];
      }
      for (int index = 0; index != 4; ++index)
        numArray[index] = 0;
      this.int_0 = num2;
      this.int_1 = num3;
    }
  }

  private sealed class Class7
  {
    internal static int smethod_0()
    {
      return Class2.Class9.smethod_0(Class2.smethod_3(typeof (Class2.Class4)), Class2.smethod_3(typeof (Class2.Class8)) ^ Class2.Class9.smethod_1(Class2.smethod_3(typeof (Class2.Class7)), Class2.Class9.smethod_2(Class2.smethod_3(typeof (Class2.Class3)), Class2.Class8.smethod_0())));
    }
  }

  private sealed class Class8
  {
    internal static int smethod_0()
    {
      return Class2.Class9.smethod_2(Class2.smethod_3(typeof (Class2.Class8)), Class2.Class9.smethod_0(Class2.smethod_3(typeof (Class2.Class5)), Class2.Class9.smethod_1(Class2.smethod_3(typeof (Class2.Class7)), Class2.Class9.smethod_2(Class2.smethod_3(typeof (Class2.Class4)), Class2.Class9.smethod_0(Class2.smethod_3(typeof (Class2.Class10)), Class2.smethod_3(typeof (Class2.Class3)))))));
    }
  }

  private static class Class9
  {
    internal static int smethod_0(int int_0, int int_1)
    {
      return int_0 ^ int_1 - -425160322;
    }

    internal static int smethod_1(int int_0, int int_1)
    {
      return int_0 - -1109792805 ^ int_1 - 1545949674;
    }

    internal static int smethod_2(int int_0, int int_1)
    {
      return int_0 ^ (int_1 - 461972554 ^ int_0 - int_1);
    }
  }

  private sealed class Class10
  {
    internal static int smethod_0()
    {
      return Class2.Class9.smethod_1(Class2.Class9.smethod_1(Class2.Class4.smethod_0(), Class2.Class9.smethod_0(Class2.smethod_3(typeof (Class2.Class10)), Class2.Class7.smethod_0())), Class2.smethod_3(typeof (Class2.Class3)));
    }
  }
}
