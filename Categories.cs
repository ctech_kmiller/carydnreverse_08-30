﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.Categories
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System.Runtime.InteropServices;

// ReSharper disable UnusedMember.Global
// ReSharper disable InconsistentNaming

namespace CaryDN
{
    [ComVisible(false)]
    public class Categories
    {
        #region Static Fields and constants

        public const string ACCESSORY = "Accessory";
        public const string DETECTOR = "Detector";
        public const string FILTER = "Filter";
        public const string GRATING = "Grating";
        public const string INTERVAL = "Interval";
        public const string LAMPS = "Lamps";
        public const string MISC = "Misc";
        public const string NIR = "Near Infra Red";
        public const string PGA = "PGA";
        public const string REGISTRATION = "Registration";
        public const string SCAN = "Scan";
        public const string SHUTTER = "Shutter";
        public const string SLIT = "Slit";
        public const string STATUS = "Status";
        public const string UVVIS = "UV Visible";
        public const string DEBUG = "Debug";
        public const string SIMULATOR = "Simulator";

        #endregion
    }
}