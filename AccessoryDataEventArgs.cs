﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.AccessoryDataEventArgs
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace CaryDN
{
    [ClassInterface(ClassInterfaceType.None)]
    [ComVisible(true)]
    [ProgId("CaryDN.AccessoryDataEventArgs")]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330007")]
    public class AccessoryDataEventArgs : CaryMessageEventArgs, IAccessoryDataEventArgs
    {
        #region Constructors

        public AccessoryDataEventArgs(Message mMessage)
            : base(mMessage)
        {
            Device = WParam;
            Value = LParamLo;
            switch (LParamHi)
            {
                case 0:
                    Response = AccessoryResponse.Status;
                    break;
                case 1:
                    Response = AccessoryResponse.Reply;
                    break;
                case 2:
                    Response = AccessoryResponse.Monitor;
                    break;
                default:
                    Response = AccessoryResponse.Unknown;
                    break;
            }
        }

        #endregion

        #region Properties and Indexers

        public int Device { get; set; }

        public AccessoryResponse Response { get; set; }

        public int Value { get; set; }

        #endregion

        #region Interface Implementations

        public override string ToString()
        {
            return $"Device={(object) Device}, Response={(object) Response}, Value={(object) Value}";
        }

        #endregion
    }
}