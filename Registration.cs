﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.Registration
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace CaryDN
{
    [ProgId("CaryDN.Registration")]
    [ComDefaultInterface(typeof(IRegistration))]
    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.AutoDual)]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3330004")]
    public class Registration : IRegistration
    {
        #region Fields

        // ReSharper disable once UnusedMember.Local
        private DateTime _dateTime1 = DateTime.MinValue;
        private readonly string _registrySubkey = "Software\\microsoft\\WinMgrPlusEx\\Marlinka";

        #endregion

        #region Constructors

        public Registration()
        {
            method_0();
        }

        #endregion

        #region Properties and Indexers

        public string Key { get; set; }

        public int TrialDays { get; } = 100;

        [Description("The number of trial days remaining.")]
        [DisplayName("Trial Days Left")]
        public int TrialDaysLeft
        {
            get
            {
                if (TrialDaysUsed > TrialDays || TrialDaysUsed == TrialDays)
                    return 0;
                return TrialDays - TrialDaysUsed;
            }
        }

        public int TrialDaysUsed => TimeSpan.FromTicks(DateTime.Now.Ticks - TrialStart.Ticks).Days;

        public DateTime TrialEnd => TrialStart.AddDays(TrialDays);

        public DateTime TrialStart { get; private set; } = DateTime.MinValue;

        #endregion

        #region Interface Implementations

        public void Load()
        {
        }

        public override string ToString()
        {
            return string.Format("{0} Days Left", TrialDaysLeft);
        }

        #endregion

        #region Other Members

        // ReSharper disable once UnusedMember.Local
        private void DeleteSubkey()
        {
            Registry.CurrentUser.DeleteSubKey(_registrySubkey);
        }

        private void method_0()
        {
            var registryKey1 = method_2();
            if (registryKey1 == null)
            {
                TrialStart = DateTime.Now;
                var registryKey2 = method_1();
                registryKey2.SetValue("TrialStart", TrialStart.Ticks);
                registryKey2.Close();
            }
            else
            {
                TrialStart = new DateTime(Convert.ToInt64(registryKey1.GetValue("TrialStart")));
                registryKey1.Close();
            }
        }

        private RegistryKey method_1()
        {
            return Registry.CurrentUser.CreateSubKey(_registrySubkey);
        }

        private RegistryKey method_2()
        {
            return Registry.CurrentUser.OpenSubKey(_registrySubkey);
        }

        #endregion
    }
}