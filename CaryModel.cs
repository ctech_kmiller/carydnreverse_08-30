﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.CaryModel
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

namespace CaryDN
{
    public enum CaryModel
    {
        Unknown = -1,
        Cary50 = 0,
        Cary100 = 1,
        Cary200 = 2,
        Cary300 = 3,
        Cary400 = 4,
        Cary500 = 5,
        Cary4000 = 6,
        Cary5000 = 7,
        // ReSharper disable once InconsistentNaming
        Cary6000i = 8,
        DeepUV = 9,
        Cary60 = 10 // 0x0000000A
    }
}