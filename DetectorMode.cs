﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.DetectorMode
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

namespace CaryDN
{
    public enum DetectorMode
    {
        Auto,
        UltraVioletVisible,
        NearInfraRed
    }
}