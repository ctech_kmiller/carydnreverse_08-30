﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.InstrumentEventDispId
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System.Runtime.InteropServices;

namespace CaryDN
{
    [ComVisible(true)]
    public enum InstrumentEventDispId
    {
        Connected = 1,
        Connecting = 2,
        DataReceived = 3,
        Disconnected = 4,
        Disconnecting = 5,
        Error = 6,
        ValueInReceived = 7,
        Resetting = 8,
        StatusChanged = 9,
        CaryInfoReceived = 10, // 0x0000000A
        OnlineChanged = 11,    // 0x0000000B
        ParamWL = 12           // 0x0000000C
    }
}