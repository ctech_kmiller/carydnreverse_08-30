﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.WinUvEnvironment
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace CaryDN
{
    [ComVisible(false)]
    public class WinUvEnvironment
    {
        #region Properties and Indexers

        public static DirectoryInfo AssemblyLocation =>
            new FileInfo(Assembly.GetExecutingAssembly().Location).Directory;

        public static string CaryWinUVFolder => AppDomain.CurrentDomain.BaseDirectory;

        #endregion

        #region Other Members

        public static void CheckFileExists(string fileName)
        {
            var fileName1 = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);
            if (!new FileInfo(fileName1).Exists)
                throw new FileNotFoundException($"File not found: {(object) fileName1}");
        }

        // ReSharper disable once UnusedMember.Global
        public static void CheckFiles()
        {
            CheckFileExists("Cary32.dll");
            CheckFileExists("FCary.dll");
            CheckFileExists("Scary.dll");
        }

        public static bool FileExists(string fileName)
        {
            return new FileInfo(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName)).Exists;
        }

        #endregion
    }
}