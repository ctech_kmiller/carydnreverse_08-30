﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.IInstrumentEvents
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System;
using System.Runtime.InteropServices;

namespace CaryDN
{
    [Guid("31F0B69F-E4BC-4328-9B0C-2DFDE3340001")]
    [ComVisible(true)]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IInstrumentEvents
    {
        [DispId(1)]
        void Connected(object sender, EventArgs e);

        [DispId(2)]
        void Connecting(object sender, EventArgs e);

        [DispId(3)]
        void DataReceived(object sender, DataEventArgs e);

        [DispId(4)]
        void Disconnected(object sender, EventArgs e);

        [DispId(5)]
        void Disconnecting(object sender, EventArgs e);

        [DispId(6)]
        void Error(object sender, ErrorEventArgs e);

        [DispId(7)]
        void ValueInReceived(object sender, ValueInEventArgs e);

        [DispId(8)]
        void Resetting(object sender, EventArgs e);

        [DispId(9)]
        void StatusChanged(object sender, StatusEventArgs e);

        [DispId(10)]
        void CaryInfoReceived(object sender, CaryInfoEventArgs e);

        [DispId(11)]
        void OnlineChanged(object sender, BoolValueEventArgs e);

        [DispId(12)]
        void ParamWL(object sender, CaryMessageEventArgs e);
    }
}