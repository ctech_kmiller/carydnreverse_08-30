﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyDescription("StarTek Cary Dot Net Library")]
[assembly: AssemblyCompany("StarTek Technology")]
[assembly: AssemblyCopyright("Copyright ©  2018 StarTek Technology")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyTitle("CaryDN")]
[assembly: ComVisible(false)]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyFileVersion("1.0.7.0")]
[assembly: AssemblyProduct("CaryDN")]
[assembly: Guid("093f405c-0d67-4737-8c2b-87c146efe500")]
[assembly: AssemblyVersion("1.0.7.0")]
