﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.InstrumentStatusCode
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System.Text;

// ReSharper disable UnusedMember.Global
// ReSharper disable InconsistentNaming

namespace CaryDN
{
    public class InstrumentStatusCode
    {
        #region Static Fields and constants

        public const uint STATUS_BUSY = 1;
        public const uint STATUS_RESETTING = 2;
        public const uint STATUS_COLLECTING = 4;
        public const uint STATUS_SLEWING = 8;
        public const uint STATUS_DRIVING_SLIT = 16;
        public const uint STATUS_CALIB_PGA = 32;
        public const uint STATUS_WAITING_FOR_USER = 64;
        public const uint STATUS_LIGHTING_UV_LAMP = 256;
        public const uint STATUS_LIGHTING_VIS_LAMP = 512;
        public const uint STATUS_CHANGING_GRATING = 1024;
        public const uint STATUS_CHANGING_SOURCE = 2048;
        public const uint STATUS_CHANGING_FILTER = 4096;
        public const uint STATUS_OFFLINE = 8192;
        public const uint STATUS_END_OF_COLLECT = 16384;
        public const uint STATUS_CHANGING_DET = 65536;
        public const uint STATUS_INIT_WAVELENGTH = 131072;
        public const uint STATUS_INIT_MONO = 262144;
        public const uint STATUS_INIT_SLIT = 524288;
        public const uint STATUS_CALIB_WAVELENGTH = 1048576;
        public const uint STATUS_CALIB_PM_ZERO = 2097152;
        public const uint STATUS_TIME_OUT = 4194304;
        public const uint STATUS_SNR_TIMEOUT = 8388608;
        public const uint STATUS_CALIB_NIR_DET = 16777216;
        public const uint STATUS_LID_OPEN = 33554432;
        public const uint STATUS_OVERHEAT = 67108864;
        public const uint STATUS_WAITING_FOR_CHOPPER = 134217728;
        public const uint STATUS_DATA_OVERRANGE = 268435456;
        public const uint STATUS_DATA_UNDERRANGE = 536870912;

        #endregion

        #region Other Members

        public static bool BitTest(uint statusCode, uint mask)
        {
            return ((int) statusCode & (int) mask) != 0;
        }

        public static string ToString(int statusCode)
        {
            return ToString((uint) statusCode);
        }

        public static string ToString(uint statusCode)
        {
            if (statusCode == 0U)
                return "Ready";
            var stringBuilder = new StringBuilder();
            if (BitTest(statusCode, 1U))
                stringBuilder.Append("Busy, ");
            if (BitTest(statusCode, 2U))
                stringBuilder.Append("Resetting, ");
            if (BitTest(statusCode, 4U))
                stringBuilder.Append("Collecting, ");
            if (BitTest(statusCode, 8U))
                stringBuilder.Append("Slewing, ");
            if (BitTest(statusCode, 16U))
                stringBuilder.Append("Driving slit, ");
            if (BitTest(statusCode, 32U))
                stringBuilder.Append("Calibrating PGA, ");
            if (BitTest(statusCode, 64U))
                stringBuilder.Append("Waiting for user, ");
            if (BitTest(statusCode, 256U))
                stringBuilder.Append("Lighting UV lamp, ");
            if (BitTest(statusCode, 512U))
                stringBuilder.Append("Lighting VIS lamp, ");
            if (BitTest(statusCode, 1024U))
                stringBuilder.Append("Changing grating, ");
            if (BitTest(statusCode, 2048U))
                stringBuilder.Append("Changing source, ");
            if (BitTest(statusCode, 4096U))
                stringBuilder.Append("Changing filter, ");
            if (BitTest(statusCode, 8192U))
                stringBuilder.Append("Cary offline, ");
            if (BitTest(statusCode, 65536U))
                stringBuilder.Append("Changing detector, ");
            if (BitTest(statusCode, 131072U))
                stringBuilder.Append("Init wavelength, ");
            if (BitTest(statusCode, 262144U))
                stringBuilder.Append("Init mono, ");
            if (BitTest(statusCode, 524288U))
                stringBuilder.Append("Init slit, ");
            if (BitTest(statusCode, 1048576U))
                stringBuilder.Append("Calibrating wavelength, ");
            if (BitTest(statusCode, 2097152U))
                stringBuilder.Append("Calibrating PM zero, ");
            if (BitTest(statusCode, 8388608U))
                stringBuilder.Append("SNR timeout, ");
            if (BitTest(statusCode, 16777216U))
                stringBuilder.Append("Calibrating NIR detector, ");
            if (BitTest(statusCode, 33554432U))
                stringBuilder.Append("Lid open, ");
            if (BitTest(statusCode, 67108864U))
                stringBuilder.Append("Over heat, ");
            if (BitTest(statusCode, 134217728U))
                stringBuilder.Append("Waiting for chopper, ");
            if (BitTest(statusCode, 268435456U))
                stringBuilder.Append("Data overrange, ");
            if (BitTest(statusCode, 536870912U))
                stringBuilder.Append("Data underrange, ");
            if (BitTest(statusCode, 16384U))
                stringBuilder.Append("End of Collect, ");
            var chArray = new[] {',', ' '};
            var str = stringBuilder.ToString().Trim(chArray);
            if (stringBuilder.Length == 0)
                str = $"Status = {(object) statusCode}";
            return str;
        }

        #endregion
    }
}