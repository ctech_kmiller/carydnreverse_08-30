﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.IAccessory
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System.Runtime.InteropServices;

namespace CaryDN
{
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3331002")]
    [ComVisible(true)]
    public interface IAccessory
    {
        bool Condition(int device, int value, int state);

        bool Drive(int device, int condition, int device2, int value, int state);

        bool EnableDrum(bool value);

        bool EnableXSlide(bool value);

        float GetParameter(int item);

        bool GoBusy(int device, int value, int state);

        bool GotoCell(int cellNumber);

        bool Limits(int device, int lowerLimit, int upperLimit);

        int Measure(int device);

        bool Monitor(int device1, int device2, int device3, int device4, int timeVal);

        bool Ramp(int device, int rate);

        bool RampA(int device, int rate);

        bool Reset(int item);

        bool SetParameter(int item, float value);

        bool Stop();

        void WaitNotBusy();
    }
}