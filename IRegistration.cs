﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.IRegistration
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System;
using System.Runtime.InteropServices;

namespace CaryDN
{
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    [ComVisible(true)]
    [Guid("31F0B69F-E4BC-4328-9B0C-1DFDE3331004")]
    public interface IRegistration
    {
        void Load();

        string ToString();

        int TrialDays { get; }

        int TrialDaysLeft { get; }

        int TrialDaysUsed { get; }

        DateTime TrialEnd { get; }

        DateTime TrialStart { get; }

        string Key { get; set; }
    }
}