﻿// Decompiled with JetBrains decompiler
// Type: CaryDN.IDataEventArgs
// Assembly: CaryDN, Version=1.0.7.0, Culture=neutral, PublicKeyToken=null
// MVID: 84490585-9449-4E2F-918D-08A8C71DC59E
// Assembly location: C:\src\Decompile\Agilent\CTech_30-Aug-2018\Cary_WinUV_Apps\output\CaryDN.dll

using System.Runtime.InteropServices;

namespace CaryDN
{
    [Guid("31F0B69F-E4BC-4328-9B0C-2DFDE3331019")]
    [ComVisible(false)]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface IDataEventArgs
    {
        float Abs { get; }

        float FrontBeam { get; set; }

        float RearBeam { get; set; }

        string ToString();

        float Wavelength { get; set; }
    }
}